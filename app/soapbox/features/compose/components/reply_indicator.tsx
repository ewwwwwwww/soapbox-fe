import React, { useState } from 'react';
import { Popover } from 'react-text-selection-popover';

import AttachmentThumbs from 'soapbox/components/attachment-thumbs';
import { Stack, Text } from 'soapbox/components/ui';
import IconButton from 'soapbox/components/ui/icon-button/icon-button';
import AccountContainer from 'soapbox/containers/account_container';
import { isRtl } from 'soapbox/rtl';

import type { Status } from 'soapbox/types/entities';

interface IReplyIndicator {
  status?: Status,
  onCancel?: () => void,
  onHighlight?: (textContent: String | undefined) => void,
  hideActions: boolean,
}

function clearSelection() {
  const selection = window.getSelection();
  if (selection && selection.removeAllRanges !== null) {
    selection.removeAllRanges();
  }
}

const ReplyIndicator: React.FC<IReplyIndicator> = ({ status, hideActions, onCancel, onHighlight }) => {
  const [ref, setRef] = useState<HTMLElement>();

  const handleClick = () => {
    onCancel!();
  };

  if (!status) {
    return null;
  }

  let actions = {};
  if (!hideActions && onCancel) {
    actions = {
      onActionClick: handleClick,
      actionIcon: require('@tabler/icons/icons/x.svg'),
      actionAlignment: 'top',
      actionTitle: 'Dismiss',
    };
  }

  return (
    <Stack space={2} className='p-4 rounded-lg bg-gray-100 dark:bg-slate-700'>
      <AccountContainer
        {...actions}
        id={status.getIn(['account', 'id']) as string}
        timestamp={status.created_at}
        showProfileHoverCard={false}
      />

      <span ref={(el) => el !== null && setRef(el)}>
        <Text
          size='sm'
          dangerouslySetInnerHTML={{ __html: status.contentHtml }}
          direction={isRtl(status.search_index) ? 'rtl' : 'ltr'}
        />
      </span>

      <Popover
        target={ref}
        render={
          ({ clientRect, isCollapsed, textContent }) => {
            // eslint-disable-next-line eqeqeq
            if (clientRect == null || isCollapsed || ref == null || onHighlight == null) return null;

            const style: React.CSSProperties = {
              position: 'fixed',
              left: clientRect.x + (clientRect.width / 2),
              top: clientRect.y,
              marginTop: -3,
              transform: 'translateY(-100%)',
              background: '#212121',
              textAlign: 'center',
              color: 'white',
              zIndex: 10000,
            };

            return (
              <IconButton
                style={style}
                onClick={() => {
                  clearSelection();
                  onHighlight(textContent);
                }}
                src={require('@tabler/icons/icons/corner-left-down.svg')}
                className='text-gray-500 hover:text-gray-700 dark:text-gray-300 dark:hover:text-gray-200'
              />
            );
          }
        }
      />

      {status.media_attachments.size > 0 && (
        <AttachmentThumbs
          media={status.media_attachments}
          sensitive={status.sensitive}
        />
      )}
    </Stack>
  );
};

export default ReplyIndicator;

import classNames from 'classnames';
import * as React from 'react';

import StillImage from 'soapbox/components/still_image';
import { useSettings } from 'soapbox/hooks';

interface IAvatar {
  /** URL to the avatar image. */
  src: string,
  /** Width and height of the avatar in pixels. */
  size?: number,
  /** Extra class names for the div surrounding the avatar image. */
  className?: string,
}

/** Round profile avatar for accounts. */
const Avatar = (props: IAvatar) => {
  const { src, size: initialSize, className } = props;
  const settings = useSettings();
  const square = settings.get('squareProfilePics');

  let size: number;

  if (!initialSize) {
    switch (settings.get('sizeAvatars')) {
      case 'small':
        size = 42;
        break;
      case 'medium':
        size = 50;
        break;
      case 'large':
        size = 60;
        break;
      default:
        size = 42;
    }
  } else {
    size = initialSize;
  }

  const style: React.CSSProperties = React.useMemo(() => ({
    width: size,
    height: size,
  }), [size]);

  return (
    <StillImage
      className={classNames({
        'overflow-hidden': true,
        'rounded-full': !square,
        'rounded': square,
      }, className)}
      style={style}
      src={src}
      alt='Avatar'
    />
  );
};

export { Avatar as default };
